using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaeyrDamage : MonoBehaviour
{
    [SerializeField] PlayerItems playerItems;

    private void Start()
    {
        playerItems = GetComponentInParent<PlayerItems>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<EnemyHealth>() != null)
        {
            collision.GetComponent<EnemyHealth>().TakeDamage(playerItems.Damage);
        }
    }
}
