using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCreator : MonoBehaviour
{
    [SerializeField] private List<GameObject> EnemeysPrefabs = new List<GameObject>();
    [SerializeField] private List<Transform> CreatePoints = new List<Transform>();
    [SerializeField] private float _normalCreateTimer;
    [SerializeField] private float _createTimer;
    [SerializeField] private float _boostTime = 10;
    [SerializeField] private int _enemeyCount;
    private int _waveLVL = 1;

    void Start()
    {
        StartCoroutine(CreateNewEnemey());
        _createTimer = _normalCreateTimer; 
    }

    private void CreateEnemey()
    {
        GameObject newEnemey = Instantiate(EnemeysPrefabs[Random.Range(0, EnemeysPrefabs.Count)], CreatePoints[Random.Range(0, CreatePoints.Count)].position, Quaternion.identity);
        newEnemey.GetComponent<EnemeyCreatureMovement>().SetStats(_waveLVL);
        newEnemey.GetComponent<EnemyHealth>().SetStats(_waveLVL);
        _enemeyCount++;
        if (_enemeyCount % 10 == 0)
        {
            newEnemey.GetComponent<EnemeyCreatureMovement>().Boss();
            newEnemey.GetComponent<EnemyHealth>().Boss();
        }
    }

    private IEnumerator CreateNewEnemey()
    {
        yield return new WaitForSeconds(_createTimer);
        CreateEnemey();
        if(_enemeyCount % 20 == 0)
        {
            _waveLVL++;
        }
        if (_enemeyCount % 30 == 0)
        {
            StartCoroutine(BigVave());
        }
        StartCoroutine(CreateNewEnemey());
    }

    private IEnumerator BigVave()
    {
        _createTimer = 1;
        yield return new WaitForSeconds(_boostTime);
        _createTimer = _normalCreateTimer;
    }
}
