using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Item : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    private Canvas _mainCanvas;
    private RectTransform _rectTransform;
    private Image _imageIcon;
    public UIControler _controler;
    public Slot _currentSlot;
    public int CurrentLVL = 1;
    public ItemTypes itemType;
    public int MaxItemLVL;
    [SerializeField] protected List<Sprite> itemIcons = new List<Sprite>();
    
    public Image avatar;

    protected virtual void Start()
    {
        _rectTransform = GetComponent<RectTransform>();
        _mainCanvas = GetComponentInParent<Canvas>();
        _imageIcon = GetComponentInChildren<Image>();
        _controler = GetComponentInParent<UIControler>();
        avatar = GetComponentInChildren<Image>();
        avatar.sprite = itemIcons[CurrentLVL];
        MaxItemLVL = itemIcons.Count;
        _rectTransform.localScale = Vector3.one * 1.2f;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        var slotTransform = _rectTransform.parent;
        var currentSlot = slotTransform.gameObject.GetComponent<Slot>();
        slotTransform.SetAsLastSibling();
        _imageIcon.raycastTarget = false;
        _controler.CurrentItem = this;
        GetSlot();
        _controler.startSlot = _currentSlot;
        _rectTransform.localScale = Vector3.one *1.5f;
    }

    public void OnDrag(PointerEventData eventData)
    {
        _rectTransform.anchoredPosition += eventData.delta / _mainCanvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.localPosition = Vector3.zero;
        _imageIcon.raycastTarget = true;
        _rectTransform.localScale = Vector3.one * 1.2f;
    }

    public void GetSlot()
    {
        _currentSlot = GetComponentInParent<Slot>();
        _currentSlot.isEmpty = true;
    }

    public void SetIcon()
    {
        avatar.sprite = itemIcons[CurrentLVL];
    }

    public virtual void Effect()
    {
        transform.SetParent(_controler.startSlot.transform);
        transform.localPosition = Vector3.zero;
        _controler.startSlot.itemOnSlot = this;
        _controler.startSlot.CheckSlotState();
    }
}
