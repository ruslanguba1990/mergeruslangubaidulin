using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicAttack : MonoBehaviour
{
    [SerializeField] GameObject fireBall;
    [SerializeField] Transform firePoint;

    [SerializeField] GameObject MeteorPrefab;
    [SerializeField] Transform[] _meteoritTransform;
    [SerializeField] private float _speed;
    private PlayerItems _playerItems;

    private void Start()
    {
        _playerItems = GetComponent<PlayerItems>();
    }

    // ������ ���������� ��� ��������� �� ������� ����
    public void FireAttack()
    {
        GameObject currentBullet = Instantiate(fireBall, firePoint.position, Quaternion.identity);
        currentBullet.GetComponent<FireBall>().SetStats(_playerItems.CurrentMagicStickLvl);
        Rigidbody2D currentArrowVelocity = currentBullet.GetComponent<Rigidbody2D>();
        currentArrowVelocity.AddForce(Vector2.right * _speed, ForceMode2D.Impulse);
    }

    public void Meteor()
    {
        GameObject currentMeteor = Instantiate(MeteorPrefab, _meteoritTransform[0].position, Quaternion.identity);
        currentMeteor.GetComponent<Meteor>().SetStats(_playerItems.CurrentMagicStickLvl);
        currentMeteor.gameObject.transform.localScale = Vector2.one * 2;
        Rigidbody2D currentArrowVelocity = currentMeteor.GetComponent<Rigidbody2D>();
        currentArrowVelocity.AddForce(Vector2.right * _speed, ForceMode2D.Impulse);
    }

    public void StartRain()
    {
        StartCoroutine(MeteorRain());
    }
    private IEnumerator MeteorRain()
    {
        for (int i = 0; i < _meteoritTransform.Length;)
        {
            GameObject currentMeteor = Instantiate(MeteorPrefab, _meteoritTransform[i].position, Quaternion.identity);
            currentMeteor.GetComponent<Meteor>().SetStats(_playerItems.CurrentMagicStickLvl);
            currentMeteor.gameObject.transform.localScale = Vector2.one * 2;
            Rigidbody2D currentArrowVelocity = currentMeteor.GetComponent<Rigidbody2D>();
            currentArrowVelocity.AddForce(Vector2.right * _speed, ForceMode2D.Impulse);
            if (i <= _meteoritTransform.Length)
            {
                yield return new WaitForSeconds(0.2f);
                i++;
            }
            else
            {
                i = 0;
                StopCoroutine(MeteorRain());
            }
        }
    }
}
