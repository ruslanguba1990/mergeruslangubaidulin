using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackGrounMove : MonoBehaviour
{
    [SerializeField] private GameObject _BackgroundPrefab;
    [SerializeField] private Transform _newBackGroundPoint;
    private GameObject oldBackGround;

    private void MoveTransformForvard()
    {
        GameObject newBackGroundPart = Instantiate(_BackgroundPrefab, _newBackGroundPoint.position, Quaternion.identity);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(oldBackGround != null)
        {
            Destroy(oldBackGround);
        }
        if(collision.GetComponent<PlayerItems>() != null)
        {
            MoveTransformForvard();
            oldBackGround = gameObject;
        }
    }

}
