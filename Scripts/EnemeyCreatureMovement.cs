using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemeyCreatureMovement : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _damage;
    [SerializeField] private float _attackSpeed;
    [SerializeField] private float _currentTimeAttack;
    private GameObject _player;

    public bool _isMoving = true;
    private Animator _animator;
    void Start()
    {
        _animator = GetComponentInChildren<Animator>();
    }

    public void SetStats(int waveLVL)
    {
        _damage = waveLVL;
    }

    public void Boss()
    {
        transform.localScale = Vector3.one * 1.5f;
        _damage = _damage * 3;
    }

    void Update()
    {
        MoveEnemey();
        Attack();
    }

    private void MoveEnemey()
    {
        if (_isMoving)
        {
            float targetX = transform.position.x + -1 * _speed * Time.deltaTime;
            transform.position = new Vector2(targetX, transform.position.y);
            _animator.SetBool("isMoving", true);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerMovementController>() != null)
        {
            _isMoving = false;
            _animator.SetBool("isMoving", false);
            _animator.SetTrigger("attack");
            _player = collision.gameObject;
        }
    }

    private void Attack()
    {
        if (_isMoving == false)
        {
            if (_currentTimeAttack < _attackSpeed)
                _currentTimeAttack += Time.deltaTime;
            if (_currentTimeAttack >= _attackSpeed)
            {
                _currentTimeAttack = 0;
                _animator.SetTrigger("attack");
                //_player.GetComponent<PlayerItems>().TakeDamage(_damage); //
                _player.GetComponent<Health>().TakeDamage(_damage);
            }
        }
    }
}
