using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public float MaxHealth = 10;
    [SerializeField] private Image _healthBar;
    [SerializeField] private GameObject _deathhPanel;
    private float _currentHealth; // SerializeDelate
    private void Start()
    {
        _currentHealth = MaxHealth;
    }

    private void Update()
    {
        _healthBar.fillAmount = _currentHealth / MaxHealth;
    }

    public void TakeDamage(float damage)
    {
        _currentHealth -= damage;
        CheckIsAlive();
    }

    private void CheckIsAlive()
    {
       if(_currentHealth <= 0)
       {
           _deathhPanel.SetActive(true);
       }
    }

    public void DrinkPoison(int poisonLVL)
    {
        switch (poisonLVL)
        {
            case 1:
                _currentHealth = (int)(_currentHealth + MaxHealth / 10);
                break;

            case 2:
                _currentHealth = (int)(_currentHealth + MaxHealth / 5);
                break;
            case 3:
                _currentHealth = (int)(_currentHealth + MaxHealth / 2);
                break;

            case 4:
                _currentHealth = MaxHealth;
                break;
        }
        if (_currentHealth >= MaxHealth)
        {
            _currentHealth = MaxHealth;
        }
    }
    public void HatHeal(float heal)
    {
        _currentHealth += heal;
    }
}
