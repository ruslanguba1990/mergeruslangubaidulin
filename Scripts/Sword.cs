using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : Item
{
    protected override void Start()
    {
        base.Start();
        CurrentLVL = _controler.Shop.SwordStartLVL;
        avatar.sprite = itemIcons[CurrentLVL];
    }

    public override void Effect()
    {
        base.Effect();
        if (_controler.playerItems.CurrentSwordLvl < CurrentLVL)
        {
            _controler.playerItems.SetSword(this);
            _controler.InventorySwordIcon.sprite = avatar.sprite;
        }
    }
}
