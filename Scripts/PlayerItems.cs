using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class PlayerItems : MonoBehaviour
{
    public SpriteRenderer Hat;
    public SpriteRenderer Sword;
    public SpriteRenderer MagicStick;
    
    public float Damage = 1;
    //public float Speed = 1;

    public int CurrentSwordLvl = 1;
    public int CurrentHatLvl = 1;
    public int CurrentMagicStickLvl = 1;

    private Health playerHealth;

    [Header("MagicAttackFirePoints")]
    public Transform[] FirePoints;

    private void Start()
    {
        playerHealth = GetComponent<Health>();
    }

    public void SetSword(Sword newSword)
    {
        Sword.sprite = newSword.avatar.sprite;
        Damage = newSword.CurrentLVL;
        CurrentSwordLvl = newSword.CurrentLVL;
    }

    public void SetHat(Hat newHat)
    {
        GetComponent<Health>().MaxHealth = GetComponent<Health>().MaxHealth + newHat.CurrentLVL * 2;
        Hat.sprite = newHat.avatar.sprite;
        playerHealth.MaxHealth += newHat.CurrentLVL * 2;
        playerHealth.HatHeal(newHat.CurrentLVL * 2);
        CurrentHatLvl = newHat.CurrentLVL;
    }

    public void SetStick(MagicWeapon newStick)
    {
        MagicStick.sprite = newStick.avatar.sprite;
        CurrentMagicStickLvl = newStick.CurrentLVL;
    }
}
