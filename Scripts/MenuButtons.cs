using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    public void onClickMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void OnClickRestart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void onClickStartGame()
    {
        SceneManager.LoadScene(2);
    }
    public void onClickStartTutorial()
    {
        SceneManager.LoadScene(1);
    }
}
