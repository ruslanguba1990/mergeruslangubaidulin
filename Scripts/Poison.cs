using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poison : Item
{
    protected override void Start()
    {
        base.Start();
        //MaxItemLVL = itemIcons.Count;
    }
    public override void Effect()
    {
        _controler.playerItems.gameObject.GetComponent<Health>().DrinkPoison(CurrentLVL);
        Destroy(gameObject);
    }
}
