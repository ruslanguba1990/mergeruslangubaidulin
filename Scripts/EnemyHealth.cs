using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField] private float _maxHealth;
    [SerializeField] private float _currentHealth;
    [SerializeField] private Image _healthBar;
    [SerializeField] private Canvas _canvas;
    [SerializeField] private GameObject _coin;

    void Start()
    {
        _currentHealth = _maxHealth;
        _canvas = GetComponentInChildren<Canvas>();
        _canvas.worldCamera = Camera.main;
    }

    public void SetStats(int currentEnemyLVL)
    {
        _maxHealth = _maxHealth * currentEnemyLVL;
    }

    public void Boss()
    {
        _maxHealth = _maxHealth * 2;
    }

    private void Update()
    {
        _healthBar.fillAmount = _currentHealth / _maxHealth;
    }

    public void TakeDamage(float damage)
    {
        _currentHealth -= damage;
        _healthBar.fillAmount = _currentHealth/_maxHealth;
        CheckIsAlive();
    }

    private void CheckIsAlive() 
    {
        if (_currentHealth <= 0)
        {
            int chans = Random.Range( 0, 10 );
            if ( chans < 2 )
            Instantiate(_coin, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
