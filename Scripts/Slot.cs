using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slot : MonoBehaviour, IDropHandler
{
    public Item itemOnSlot;
    public bool isEmpty = true;
    private UIControler _controler;

    private void Start()
    {
        _controler = GetComponentInParent<UIControler>();
        CheckSlotState();
    }

    public void OnDrop(PointerEventData eventData)
    {
        var otherItemTransform = _controler.CurrentItem.transform;
        var currentItem = _controler.CurrentItem;
        otherItemTransform.SetParent(transform);
        otherItemTransform.localPosition = Vector3.zero;
        ReciveItem(currentItem);
    }
    public void ReciveItem(Item recivedItem)
    {
        if (isEmpty)
        {
            itemOnSlot = recivedItem;
            isEmpty = false;
            if(recivedItem._controler.startSlot == this)
                return;
        }

        else if(itemOnSlot.itemType == _controler.CurrentItem.itemType
            && itemOnSlot.CurrentLVL == _controler.CurrentItem.CurrentLVL
            && itemOnSlot.CurrentLVL < _controler.CurrentItem.MaxItemLVL)
        {
            itemOnSlot.CurrentLVL++;
            itemOnSlot.SetIcon();
            Destroy(recivedItem.gameObject);
        }

        else
        {
            ResiveFail();
            return;
        }
        ClearCurrentItem();
    }
    
    public void ReciveFromGenerator(Item newItem)
    {
        if (isEmpty)
        {
            itemOnSlot = newItem;
            isEmpty = false;
        }

        else if (itemOnSlot.itemType == newItem.itemType
            && itemOnSlot.CurrentLVL == newItem.CurrentLVL
            && itemOnSlot.CurrentLVL < _controler.CurrentItem.MaxItemLVL)
        {
            itemOnSlot.CurrentLVL += 2;
            itemOnSlot.SetIcon();
            Destroy(newItem.gameObject);
        }
        else
        {
            Destroy(newItem.gameObject);
        }
    }

    public void CheckSlotState()
    {
        if (itemOnSlot != null)
        {
            isEmpty = false;
        }
        else isEmpty = true;
    }

    public void ClearSlot()
    {
        itemOnSlot = null;
        CheckSlotState();
    }

    public void ClearCurrentItem()
    {
        _controler.CurrentItem = null;
        _controler.startSlot.ClearSlot();
    }

    public void ResiveFail()
    {
        _controler.CurrentItem.transform.SetParent(_controler.startSlot.transform);
        _controler.CurrentItem.transform.localPosition = Vector3.zero;
        _controler.startSlot.itemOnSlot = _controler.CurrentItem;
        Debug.Log("ReplaceFail");
        CheckSlotState();
    }
}
