using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SellItemInShop : MonoBehaviour, IDropHandler
{
    private UIControler _controler;
    private Shop _shop;

    public List<EnemyHealth> _enemys = new List<EnemyHealth>();

    void Start()
    {
        _controler = GetComponentInParent<UIControler>();
        _shop = GetComponentInParent<Shop>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        Destroy(_controler.CurrentItem.gameObject);
        _controler.CurrentItem = null;
        _shop.CurrentMoney++;
        _shop.RefreshUI();
    }
}
