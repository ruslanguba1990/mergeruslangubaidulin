using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameZoneAction : MonoBehaviour, IDropHandler
{
    private UIControler _controler;

    void Start()
    {
        _controler = GetComponentInParent<UIControler>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        _controler.CurrentItem.Effect();
    }
}
