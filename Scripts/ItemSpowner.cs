using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSpowner : MonoBehaviour
{
    public List<Item> items = new List<Item>();
    [SerializeField] private RectTransform _spownerTransform;
    [SerializeField] private RectTransform _parent;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _startSpownTime;
    private UIControler _controller;
    private RectTransform _itemTransform;
    private RectTransform _target;
    private Image _image;
    private Item _newItem;
    private List<Slot> _slots;
    private Slot _targetSlot;
    private float _currentSpownTime;
    private bool _newItemExist;

    void Start()
    {
        _currentSpownTime = _startSpownTime;
        _controller = GetComponent<UIControler>();
        _slots = _controller.MergeSlots;
        StartCoroutine(ItemSpown());
    }

    void Update()
    {
        MoveItem();
    }
    IEnumerator ItemSpown()
    {
        yield return new WaitForSeconds(_currentSpownTime);
        var emptySlots = new List<Slot>();
        foreach (var slot in _slots)
        {
            if(slot.isEmpty) 
                emptySlots.Add(slot);
        }
        if (emptySlots.Count > 0)
        {
            _targetSlot = emptySlots[Random.Range(0, emptySlots.Count)];
            _target = _targetSlot.GetComponent<RectTransform>();
            Item currentItem = Instantiate(items[Random.Range(0, items.Count)], _spownerTransform.position, Quaternion.identity, _parent);
            _itemTransform = currentItem.GetComponent<RectTransform>();
            _newItemExist = true;
            _newItem = currentItem;
            _image = _newItem.GetComponentInChildren<Image>();
            _image.raycastTarget = false;
            StartCoroutine(ItemSpown());
        }
        else
        {
            yield return new WaitForSeconds(_currentSpownTime * 10);
            StartCoroutine(ItemSpown());
        }
    }
    private void MoveItem()
    {
        if (_newItemExist == true)
        {
            _itemTransform.anchoredPosition = Vector2.MoveTowards(_itemTransform.anchoredPosition, _target.anchoredPosition, _moveSpeed * Time.deltaTime);

            if (Vector2.Distance(_newItem.transform.position, _target.position) < 0.5f)
            {
                _itemTransform.SetParent(_target);
                _itemTransform.localPosition = Vector3.zero;
                _targetSlot.ReciveFromGenerator(_newItem);
                _newItemExist = false;
                _newItem = null;
                _image.raycastTarget = true;
            }
        }
    }
}
