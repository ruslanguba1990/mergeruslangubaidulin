using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hat : Item
{
    protected override void Start()
    {
        base.Start();
        CurrentLVL = _controler.Shop.HatStartLVL;
        MaxItemLVL = itemIcons.Count;
        avatar.sprite = itemIcons[CurrentLVL];
    }

    public override void Effect()
    {
        base.Effect();
        if (_controler.playerItems.CurrentHatLvl < CurrentLVL)
        {
            _controler.playerItems.SetHat(this);
            _controler.InventoryHatIcon.sprite = avatar.sprite;
        }
    }
}
