using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemTypes: int
{
    Sward,
    MagickStick,
    Heat,
    Poison,
    FireMagic,
    Special
}
