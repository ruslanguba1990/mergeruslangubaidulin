using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireMagic : Item
{
    MagicAttack _magicAttack;
    protected override void Start()
    { 
        base.Start();
        _magicAttack = _controler.playerItems.gameObject.GetComponent<MagicAttack>();
        //MaxItemLVL = itemIcons.Count;
    }

    public override void Effect()
    {
        switch (this.CurrentLVL) 
        {
             case 1:
                _magicAttack.FireAttack();
                break; 
            case 2:
                _magicAttack.Meteor();
                break;
            case 3:
                _magicAttack.StartRain();
                break;
        }
        Destroy(gameObject);
    }
}
