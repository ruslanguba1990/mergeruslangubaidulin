using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
    private Animator _playerAnimator;
    private PlayerItems _items;
    [SerializeField] private Shop shop;
    [SerializeField] private float speed;
    [SerializeField] private float _attackSpeed;
    [SerializeField] private float _attackRange;
    [SerializeField] private float _currentTimeAttack;
    [SerializeField] private LayerMask _enemeyLayer;
    [SerializeField] Transform _swaordAttackPoint;
    private AudioContoller _soundContoller;
    public bool _isMoving =  true;

    void Start()
    {
        _playerAnimator = GetComponentInChildren<Animator>();
        _items = GetComponent<PlayerItems>();
        _playerAnimator.SetBool("isMoving", true);
        _soundContoller = GetComponent<AudioContoller>();
    }

    void Update()
    {
        MovePlaeyr();
        AttackTimer();
    }

    private void MovePlaeyr()
    {
        if (_isMoving)
        {
            float targetX = transform.position.x + speed * Time.deltaTime;
            transform.position = new Vector2(targetX, transform.position.y);
        }
    }

    private void Attack()
    {
        _playerAnimator.SetTrigger("attack");
        Invoke("AttackDependOnAnim", 1);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<EnemyHealth>() != null)
        {
            _isMoving = false;
            _playerAnimator.SetBool("isMoving", false);
        }

        if(collision.GetComponent<Coin>() != null)
        {
            shop.CurrentMoney++;
            shop.RefreshUI();
        }
    }

    private void AttackTimer()
    {
        if (_isMoving == false)
        {
            if (_currentTimeAttack < _attackSpeed)
                _currentTimeAttack += Time.deltaTime;
            if (_currentTimeAttack >= _attackSpeed)
            {
                Collider2D[] EnemiesLeft = Physics2D.OverlapCircleAll(_swaordAttackPoint.position, _attackRange, _enemeyLayer);
                if (EnemiesLeft.Length != 0)
                {
                    _currentTimeAttack = 0;
                    Attack();
                }
                else _isMoving = true;
                _playerAnimator.SetBool("isMoving", true);
            }
        }
    }

    private void AttackDependOnAnim()
    {
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(_swaordAttackPoint.position, _attackRange, _enemeyLayer);
        foreach (Collider2D enemey in hitEnemies)
        {
            enemey.GetComponent<EnemyHealth>().TakeDamage(_items.Damage);
        }
        _soundContoller.PlaySwordHit(); 
    }


}
