using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor : FireBall
{
    public override void SetStats(int magicStickLvl)
    {
        _damage = magicStickLvl * 2;
        _explosionRadius = magicStickLvl;
    }

    private void Update()
    {
        if (transform.position.y <= 4)
        {
            rb.velocity = Vector2.zero;
            rb.gravityScale = 0;
            Explosion();
        }
    }

}
