using Unity.VisualScripting;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    protected float _damage;
    protected float _explosionRadius;
    [SerializeField] private GameObject explosion;
    protected Rigidbody2D rb;

    public virtual void SetStats(int magicStickLvl)
    {
        _damage = magicStickLvl * 2;
        _explosionRadius = magicStickLvl/2;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<EnemyHealth>() != null)
        {
            rb.velocity = Vector3.zero;
            Explosion();
        }
    }

    protected void Explosion()
    {
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(transform.position, _explosionRadius);
        foreach (Collider2D enemey in hitEnemies)
        {
            if(enemey.GetComponent<EnemyHealth>() != null)
            enemey.GetComponent<EnemyHealth>().TakeDamage(_damage);
        }
        explosion.SetActive(true);
        Destroy(gameObject, 1f);
    }
}
