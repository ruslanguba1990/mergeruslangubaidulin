using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioContoller : MonoBehaviour
{
    [SerializeField] private AudioClip SwordAttackSound, hitSound2, deathSound;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySwordHit()
    {
        audioSource.PlayOneShot(SwordAttackSound);
        audioSource.PlayOneShot(hitSound2);
    }

}
