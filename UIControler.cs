using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIControler : MonoBehaviour
{
    public Item CurrentItem;
    public List<Slot> MergeSlots = new List<Slot>();
    public Slot startSlot;
    public PlayerItems playerItems;
    public Shop Shop; // GetComponent
    [Header("Inventory Equpt Items")]
    public Image InventorySwordIcon;
    public Image InventoryStickIcon;
    public Image InventoryHatIcon;
}
