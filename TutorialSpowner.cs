using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class TutorialSpowner : MonoBehaviour
{
    // ���� ����� ������ � ������ ��� ���������� �������� ��������,
    // ��� ������������ ������ �� ������� � ���� � ������� ����������

    public List<Item> items = new List<Item>();
    [SerializeField] private RectTransform _spownerTransform;
    [SerializeField] private RectTransform _parent;
    [SerializeField] private float _moveSpeed;
    [SerializeField] private float _startSpownTime;
    private UIControler _controller;
    private RectTransform _itemTransform;
    private RectTransform _target;
    private Image _image;
    private Item _newItem;
    [SerializeField] private List<Slot> _slots;
    private Slot _targetSlot;
    private float _currentSpownTime;
    private bool _newItemExist;
    int indexItem = 0;
    int indexSlot = 0;

    [SerializeField] Animator _plaerAnimator;
    [SerializeField] PlayerMovementController _playerMovementController;
    [SerializeField] EnemeyCreatureMovement _enemeyMovement;
    [SerializeField] List<GameObject> advise = new List<GameObject>();
    [SerializeField] List<GameObject> itemIcons = new List<GameObject>();
    public int _currentAdvise;
    private int indexItenIcon;
    [SerializeField] private GameObject iconsPanel;
    [SerializeField] private GameObject magicAdvisePanel1;
    [SerializeField] private GameObject magicAdvisePanel2;
    [SerializeField] private GameObject magicAdvisePanel3;
    [SerializeField] private GameObject finTutorial;
    [SerializeField] private ItemSpowner _itemCreator;
    [SerializeField] private EnemyCreator _enemyCreator;


    void Start()
    {
        _currentSpownTime = _startSpownTime;
        _itemCreator = GetComponent<ItemSpowner>();
        _controller = GetComponent<UIControler>();
        advise[_currentAdvise].SetActive(true);
        _enemyCreator.enabled = false;
        _plaerAnimator.SetBool("isMoving", false);
    }

    void Update()
    {
        MoveItem();
    }
    IEnumerator ItemSpown()
    {
        yield return new WaitForSeconds(_currentSpownTime);
        var emptySlots = new List<Slot>();
        foreach (var slot in _slots)
        {
            if (slot.isEmpty)
                emptySlots.Add(slot);
        }
        if (emptySlots.Count > 0)
        {
            _targetSlot = emptySlots[Random.Range(0, emptySlots.Count)];
            _target = _targetSlot.GetComponent<RectTransform>();
            Item currentItem = Instantiate(items[Random.Range(0, items.Count)], _spownerTransform.position, Quaternion.identity, _parent);
            _itemTransform = currentItem.GetComponent<RectTransform>();
            _newItemExist = true;
            _newItem = currentItem;
            _image = _newItem.GetComponentInChildren<Image>();
            _image.raycastTarget = false;
            StartCoroutine(ItemSpown());
        }
        else
        {
            yield return new WaitForSeconds(_currentSpownTime * 10);
            StartCoroutine(ItemSpown());
        }
    }
    private void MoveItem()
    {
        if (_newItemExist == true)
        {
            _itemTransform.anchoredPosition = Vector2.MoveTowards(_itemTransform.anchoredPosition, _target.anchoredPosition, _moveSpeed * Time.deltaTime);

            if (Vector2.Distance(_newItem.transform.position, _target.position) < 0.5f)
            {
                _itemTransform.SetParent(_target);
                _itemTransform.localPosition = Vector3.zero;
                _targetSlot.ReciveFromGenerator(_newItem);
                _newItemExist = false;
                _newItem = null;
                _image.raycastTarget = true;
            }
        }
    }

    private void InstItem(int indexItem, int indexSlot)
    {
            _targetSlot = _slots[indexSlot];
            _target = _targetSlot.GetComponent<RectTransform>();
            Item currentItem = Instantiate(items[indexItem], _spownerTransform.position, Quaternion.identity, _parent);
            _itemTransform = currentItem.GetComponent<RectTransform>();
            _newItemExist = true;
            _newItem = currentItem;
            _image = _newItem.GetComponentInChildren<Image>();
            _image.raycastTarget = false;
    }

    public void FirstButtonClick()
    {
        InstItem(indexItem, indexSlot);
        indexSlot++;
        ++_currentAdvise;

        advise[_currentAdvise - 1].SetActive(false);
        advise[_currentAdvise].SetActive(true);

        if (indexSlot % 2 == 0) 
            indexItem++;

        if (_currentAdvise < 3 && _currentAdvise > 0)
            itemIcons[0].SetActive(true);

        if (_currentAdvise < 5 && _currentAdvise >= 3)
        {
            itemIcons[0].SetActive(false);
            itemIcons[1].SetActive(true);
        }
        if (_currentAdvise < 7 && _currentAdvise >= 5)
        {
            itemIcons[1].SetActive(false);
            itemIcons[2].SetActive(true);
        }
        if (_currentAdvise == 7)
        {
            magicAdvisePanel1.SetActive(false);
            magicAdvisePanel2.SetActive(true);
            iconsPanel.SetActive(false);
        }
    }

    public void SecondButtonClick()
    {
        ++_currentAdvise;
        advise[_currentAdvise - 1].SetActive(false);
        advise[_currentAdvise].SetActive(true);
        if(_currentAdvise == 10)
        {
            magicAdvisePanel2.SetActive(false);
            magicAdvisePanel3.SetActive(true);
        }
    }

    public void LastButtonClick()
    {
        _enemeyMovement.enabled = true;
        _enemyCreator.enabled = true;
        _playerMovementController.enabled = true;
        _itemCreator.enabled = true;
        _plaerAnimator.SetBool("isMoving", true);
        StartCoroutine(TutorialTimer());
        magicAdvisePanel3.SetActive(false);
    }

    public void EndTutorial()
    {
       SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

     IEnumerator TutorialTimer()
    {
        yield return new WaitForSeconds(30);
        finTutorial.SetActive(true);
    }
}
