using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicWeapon : Item
{
    protected override void Start()
    {
        base.Start();
        MaxItemLVL = itemIcons.Count;
        CurrentLVL = _controler.Shop.StuffStartLVL;
        avatar.sprite = itemIcons[CurrentLVL];
    }

    public override void Effect()
    {
        base.Effect();
        if (_controler.playerItems.CurrentMagicStickLvl < CurrentLVL)
        { 
            _controler.playerItems.SetStick(this);
            _controler.InventoryStickIcon.sprite = avatar.sprite;
            _controler.InventoryStickIcon.SetNativeSize();
            _controler.InventoryStickIcon.rectTransform.localScale = Vector3.one/2;
        }
    }
}
