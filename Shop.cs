using Microsoft.Cci;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public int SwordStartLVL;
    public int StuffStartLVL;
    public int HatStartLVL;
    public int CurrentMoney;

    [Header("Sprites")]
    [SerializeField] private Sprite[] _swordsIcons;
    [SerializeField] private Sprite[] _stuffsIcons;
    [SerializeField] private Sprite[] _hatIcons;

    [Header("Icons")]
    [SerializeField] private Image _swordIcon;
    [SerializeField] private Image _stuffIcon;
    [SerializeField] private Image _hatIcon;

    [Header("Sprites")]
    [SerializeField] private Text _swordLVLUPCostText;
    [SerializeField] private Text _stuffLVLUPCostText;
    [SerializeField] private Text _hatLVLUPCostText;
    [SerializeField] private Text _cuurrentMoneyText;
    [SerializeField] private Text _gamePanelMoneyText;

    private int _swordLVLUPCost = 3;
    private int _stuffLVLUPCost = 3;
    private int _hatLVLUPCost = 3;


    void Start()
    {
        RefreshUI();
    }

    public void IncraceSwordStartLVL()
    {
        if (_swordLVLUPCost <= CurrentMoney)
        {
            CurrentMoney -= _swordLVLUPCost;
            SwordStartLVL++;
            _swordIcon.sprite = _swordsIcons[SwordStartLVL];
            _swordLVLUPCost = _swordLVLUPCost * 2;
            _swordLVLUPCostText.text = _swordLVLUPCost.ToString();
            RefreshUI();
        }
    }

    public void IncraceStuffStartLVL()
    {
        if (_stuffLVLUPCost <= CurrentMoney)
        {
            CurrentMoney -= _stuffLVLUPCost;
            StuffStartLVL++;
            _stuffIcon.sprite = _stuffsIcons[StuffStartLVL];
            _stuffIcon.SetNativeSize();
            _stuffIcon.rectTransform.localScale = Vector2.one/2;
            _stuffLVLUPCost = _stuffLVLUPCost * 2;
            _stuffLVLUPCostText.text += _stuffLVLUPCost.ToString();
            RefreshUI();
        }
    }

    public void IncraceHatStartLVL()
    {
        if(_hatLVLUPCost <= CurrentMoney)
        {
            CurrentMoney -= _hatLVLUPCost;
            HatStartLVL++;
            _hatIcon.sprite = _hatIcons[HatStartLVL];
            _hatLVLUPCost = _hatLVLUPCost * 2;
            _hatLVLUPCostText.text = _hatLVLUPCost.ToString();
            RefreshUI();
        }
    }

    public void RefreshUI()
    {
        _swordLVLUPCostText.text = _swordLVLUPCost.ToString();
        _stuffLVLUPCostText.text = _stuffLVLUPCost.ToString();
        _hatLVLUPCostText.text = _hatLVLUPCost.ToString();
        _cuurrentMoneyText.text = CurrentMoney.ToString();
        _gamePanelMoneyText.text = CurrentMoney.ToString();
    }

    public void OnClickContinue()
    {
        Time.timeScale = 1;
    }

    public void OnClickPause()
    {
        Time.timeScale = 0;
    }
}
